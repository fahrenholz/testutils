package testutils

import (
    "github.com/jordan-wright/email"
    "net/smtp"
    "testing"
)

const succeed = "\u2713"
const failed = "\u2717"

func TestMailhogApiClient_GetMessages(t *testing.T) {
    t.Log("should be able to retrieve all currently available messages")
    //given
    hog := NewMailhogApiClient("http://localhost:8025", "user", "password")
    err := hog.Flush()
    if err != nil {
        t.Fatalf("\t%s: (preparation) could not flush all messages", failed)
    }
    err = sendTestMailToMailhog("TestGetMessages", "Message1", "test <test@example.com>", []string{"user@example.com"})
    if err != nil {
        t.Fatalf("\t%s: (preparation) could not send first message to mailhog", failed)
    }
    //when
    coll, err := hog.GetMessages()
    //then
    if err != nil {
        t.Fatalf("\t%s: should not produce any error: %v", failed, err)
    } else {
        t.Logf("\t%s: should not produce any error.", succeed)
    }
    if coll.TotalMessages != 1 {
        t.Errorf("\t%s: should have only one message after sending one: %d", failed, coll.TotalMessages)
    } else {
        t.Logf("\t%s: should have only one message after sending one.", succeed)
    }
    if coll.Messages[0].Content.Headers["Subject"][0] != "TestGetMessages" {
        t.Errorf("\t%s: should have 'TestGetMessages' as subject: %s", failed, coll.Messages[0].Content.Headers["Subject"][0])
    } else {
        t.Logf("\t%s: should have 'TestGetMessages' as subject.", succeed)
    }
    if coll.Messages[0].Content.Body != "Message1" {
        t.Errorf("\t%s: should have 'Message1' as body: %s", failed, coll.Messages[0].Content.Body)
    } else {
        t.Logf("\t%s: should have 'Message1' as body.", succeed)
    }

    //given2
    err = sendTestMailToMailhog("TestGetMessages", "Message2", "test <test@example.com>", []string{"user@example.com"})
    if err != nil {
        t.Fatalf("\t%s: (preparation) could not send second message to mailhog", failed)
    }
    //when2
    coll, err = hog.GetMessages()
    //then2
    if err != nil {
        t.Fatalf("\t%s: should not produce any error (second attempt): %v", failed, err)
    } else {
        t.Logf("\t%s: should not produce any error (second attempt).", succeed)
    }
    if coll.TotalMessages != 2 {
        t.Errorf("\t%s: should have two messages after sending two: %d", failed, coll.TotalMessages)
    } else {
        t.Logf("\t%s: should have two messages after sending two.", succeed)
    }
}

func TestMailhogApiClient_Flush(t *testing.T) {
    t.Log("should be able to delete all messages")
    //given
    for i := 0; i < 3; i++ {
        err := sendTestMailToMailhog("TestFlush", "Message", "test <test@example.com>", []string{"user@example.com"})
        if err != nil {
            t.Fatalf("\t%s: (preparation) could not send to mailhog", failed)
        }
    }
    hog := NewMailhogApiClient("http://localhost:8025", "user", "password")
    //when
    err := hog.Flush()
    //then
    if err != nil {
        t.Errorf("\t%s: should not get any error from flush: %v", failed, err)
    } else {
        t.Logf("\t%s: should not get any error from flush.", succeed)
    }
    coll, err := hog.GetMessages()
    if err != nil {
        t.Fatalf("\t%s: should not be getting any error while retrieving messages after flush: %v", failed, err)
    }
    if coll.TotalMessages != 0 {
        t.Errorf("\t%s: should be getting back 0 messages after flush: %d", failed, coll.TotalMessages)
    } else {
        t.Logf("\t%s: should be getting back 0 messages after flush.", succeed)
    }
}

func sendTestMailToMailhog(subject string, body string, from string, to []string) error {
    e := email.NewEmail()
    e.From = from
    e.To = to
    e.Subject = subject
    e.Text = []byte(body)
    return e.Send("localhost:1025", smtp.PlainAuth("", "user", "password", "localhost"))
}
